﻿using BankManagement.DataAccessLayer.Contracts;
using BankManagement.Entities;
using BankManagement.Exceptions;

namespace BankManagment.DataAccessLayer
{
    public class CustomerDataAccessLayer : ICustomerDataAccessLayer
    {
        #region Privaee fields
        private static List<Customer> _customers;
        #endregion

        #region Constructor
        static CustomerDataAccessLayer()
        {
            _customers = new List<Customer>();
        }
        #endregion

        #region Properties
        private static List<Customer> Customers
        {
            get => _customers;
            set => _customers = value;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Return all existing customers
        /// </summary>
        /// <returns>Customer list</returns>
        public List<Customer> GetCustomers()
        {
            try
            {
                //Create a new customer list
                List<Customer> emptyCustomerList = new List<Customer>();

                //Get all customers
                Customers.ForEach(item => emptyCustomerList.Add(item.Clone() as Customer));

                //Copy all customers from the source collection into the newCustomer list
                return emptyCustomerList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Return list of customer that filtered with specified condition
        /// </summary>
        /// <param name="predicate">Lamda expression with condition</param>
        /// <returns>List of matching customers</returns>
        public List<Customer> GetCustomersByCondition(Predicate<Customer> predicate)
        {
            try
            {
                //Create a new customers list
                List<Customer> customerList = new List<Customer>();

                //filter the collection
                List<Customer> filteredCustomers = Customers.FindAll(predicate);

                //copy all customers from the source collection into the newCustomer list
                filteredCustomers.ForEach(item => filteredCustomers.Add(item.Clone() as Customer));
                return customerList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Add a new customer
        /// </summary>
        /// <param name="customer">Customer object to add</param>
        /// <returns>Return Guid of newly created customer</returns>
        public Guid AddCustomer(Customer customer)
        {
            try
            {
                //Generate new Guid
                customer.CustomerId = Guid.NewGuid();

                //Add new customer
                Customers.Add(customer);

                return customer.CustomerId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete an existing customer with customer's guid
        /// </summary>
        /// <param name="customerId">CustomerId to delete</param>
        /// <returns>Determines whether the customer is deleted or not</returns>
        public bool DeleteCustomer(Guid customerId)
        {
            try
            {
                //Delete customer by CustomerID
                if (Customers.RemoveAll(item => item.CustomerId == customerId) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update an existing customer's details
        /// </summary>
        /// <param name="customer">Customer object with updated details</param>
        /// <returns>Determines whether the customer is updated or not</returns>
        public bool UpdateCustomer(Customer customer)
        {
            try
            {
                //Find exsiting customer by CustomerID
                Customer existingCustomer = Customers.Find(item => item.CustomerId == customer.CustomerId);

                //Update all details of customer
                if (existingCustomer != null)
                {
                    existingCustomer.CustomerCode = customer.CustomerCode;
                    existingCustomer.CustomerName = customer.CustomerName;
                    existingCustomer.Address = customer.Address;
                    existingCustomer.City = customer.City;
                    existingCustomer.Country = customer.Country;
                    existingCustomer.Phone = customer.Phone;

                    //Indicate the customer is updated
                    return true;
                }
                else
                {
                    //Indicate no object is updated
                    return false;
                }
            }
            catch (CustomerExceptions)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
