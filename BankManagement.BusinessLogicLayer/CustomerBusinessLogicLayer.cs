﻿using BankManagement.BusinessLogicLayer.BAL_Contracts;
using BankManagement.Configuration;
using BankManagement.DataAccessLayer.Contracts;
using BankManagement.Entities;
using BankManagement.Exceptions;
using BankManagment.DataAccessLayer;

namespace BankManagement.BusinessLogicLayer
{
    /// <summary>
    /// Represent customer business logic
    /// </summary>
    public class CustomerBusinessLogicLayer : ICustomerBusinessLogicLayer
    {
        #region Private field 
        private ICustomerDataAccessLayer _customerDataAccessLayer;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor that initializes CustomerDataAccessLayer
        /// </summary>
        public CustomerBusinessLogicLayer()
        {
            _customerDataAccessLayer = new CustomerDataAccessLayer();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Private property that represents reference of CustomerDataAccessLayer
        /// </summary>
        private ICustomerDataAccessLayer CustomerDataAccessLayer
        {
            get => _customerDataAccessLayer;
            set => _customerDataAccessLayer = value;
        }
        #endregion

        #region MyRegion
        /// <summary>
        /// Retrieves a list of all customers.
        /// </summary>
        /// <returns>A list of Customer objects representing all customers.</returns>
        public List<Customer> GetCustomers()
        {
            try
            {
                //Invoke DAL
                return CustomerDataAccessLayer.GetCustomers();
            }
            catch (CustomerExceptions)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves a list of customers based on the specified condition.
        /// </summary>
        /// <param name="predicate">The condition to filter customers.</param>
        /// <returns>A list of Customer objects that meet the specified condition.</returns>
        public List<Customer> GetCustomersByCondition(Predicate<Customer> predicate)
        {
            try
            {
                return CustomerDataAccessLayer.GetCustomersByCondition(predicate);
            }
            catch (CustomerExceptions)
            {
                throw;
            }   
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Adds a new customer to the data store.
        /// </summary>
        /// <param name="customer">The Customer object representing the new customer to be added.</param>
        /// <returns>The unique identifier (Guid) assigned to the newly added customer.</returns>
        public Guid AddCustomer(Customer customer)
        {
            try
            {
                //Get all customer
                List<Customer> allCustomers = CustomerDataAccessLayer.GetCustomers();
                long maxCustomerNo = 0;
                foreach (var item in allCustomers)
                {
                    if (item.CustomerCode > maxCustomerNo)
                    {
                        maxCustomerNo = item.CustomerCode;
                    }
                }
                //Generate new customer's no
                if (allCustomers.Count >= 1)
                {
                    customer.CustomerCode = maxCustomerNo + 1; 
                }
                else
                {
                    customer.CustomerCode = BankManagement.Configuration.Settings.BaseCustomerNo + 1;
                }
                
                //invoke DAL
                return CustomerDataAccessLayer.AddCustomer(customer);
            }
            catch (CustomerExceptions)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Updates an existing customer in the data store.
        /// </summary>
        /// <param name="customer">The Customer object representing the updated customer information.</param>
        /// <returns>True if the customer is successfully updated; otherwise, false.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            try
            {
                return CustomerDataAccessLayer.UpdateCustomer(customer);
            }
            catch (CustomerExceptions)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes a customer from the data store based on the specified customer identifier.
        /// </summary>
        /// <param name="customerId">The unique identifier (Guid) of the customer to be deleted.</param>
        /// <returns>True if the customer is successfully deleted; otherwise, false.</returns>
        public bool DeleteCustomer(Guid customerId)
        {
            try
            {
                return CustomerDataAccessLayer.DeleteCustomer(customerId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
