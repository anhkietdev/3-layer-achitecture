﻿using BankManagement.Entities;

namespace BankManagement.BusinessLogicLayer.BAL_Contracts
{
    /// <summary>
    /// Interface that represent the customer's business logic
    /// </summary>
    public interface ICustomerBusinessLogicLayer
    {
        /// <summary>
        /// Retrieves a list of all customers.
        /// </summary>
        /// <returns>A list of Customer objects representing all customers.</returns>
        List<Customer> GetCustomers();

        /// <summary>
        /// Retrieves a list of customers based on the specified condition.
        /// </summary>
        /// <param name="predicate">The condition to filter customers.</param>
        /// <returns>A list of Customer objects that meet the specified condition.</returns>
        List<Customer> GetCustomersByCondition(Predicate<Customer> predicate);

        /// <summary>
        /// Adds a new customer to the data store.
        /// </summary>
        /// <param name="customer">The Customer object representing the new customer to be added.</param>
        /// <returns>The unique identifier (Guid) assigned to the newly added customer.</returns>
        Guid AddCustomer(Customer customer);

        /// <summary>
        /// Updates an existing customer in the data store.
        /// </summary>
        /// <param name="customer">The Customer object representing the updated customer information.</param>
        /// <returns>True if the customer is successfully updated; otherwise, false.</returns>
        bool UpdateCustomer(Customer customer);

        /// <summary>
        /// Deletes a customer from the data store based on the specified customer identifier.
        /// </summary>
        /// <param name="customerId">The unique identifier (Guid) of the customer to be deleted.</param>
        /// <returns>True if the customer is successfully deleted; otherwise, false.</returns>
        bool DeleteCustomer(Guid customerId);
    }
}
