﻿using BankManagement.BusinessLogicLayer;
using BankManagement.BusinessLogicLayer.BAL_Contracts;
using BankManagement.Entities;

namespace BankManagement.Presentation
{
    static class CustomerPresentation
    {
        internal static void AddCustomer()
        {
            try
            {
                //Create an object of customer
                Customer customer = new Customer();

                //Read all details from the user  
                Console.WriteLine("\n*****ADD CUSTOMER*****");
                Console.Write("Customer Name: ");
                customer.CustomerName = Console.ReadLine();
                Console.Write("Address: ");
                customer.Address = Console.ReadLine();
                Console.Write("City: ");
                customer.City = Console.ReadLine();
                Console.Write("Country: ");
                customer.Country = Console.ReadLine();
                Console.Write("Phone: ");
                customer.Phone = Console.ReadLine();

                //Create BL object
                ICustomerBusinessLogicLayer customerBusinessLogicLayer = new CustomerBusinessLogicLayer();
                Guid newGuid = customerBusinessLogicLayer.AddCustomer(customer);

                List<Customer> matchingCustomers = customerBusinessLogicLayer.GetCustomersByCondition(item => item.CustomerId == newGuid);
                if (matchingCustomers.Count >= 1)
                {
                    Console.WriteLine("New Customer's Code: " + matchingCustomers[0].CustomerCode);
                    Console.WriteLine("Customer added.\n");
                }
                else
                {
                    Console.WriteLine("Customer not added.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }

        internal static void ViewCustomers()
        {
            try
            {
                ICustomerBusinessLogicLayer customerBusinessLogicLayer = new CustomerBusinessLogicLayer();

                List<Customer> allCustomers = customerBusinessLogicLayer.GetCustomers();
                Console.WriteLine("\n*****ALL CUSTOMERS*****");

                foreach (Customer item in allCustomers)
                {
                    Console.WriteLine("CustomerCode: " + item.CustomerCode);
                    Console.WriteLine("CustomerName: " + item.CustomerName);
                    Console.WriteLine("Address: " + item.Address);
                    Console.WriteLine("City: " + item.City);
                    Console.WriteLine("Country: " + item.Country);
                    Console.WriteLine("Phone: " + item.Phone);
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetType());
            }
        }
    }
}
