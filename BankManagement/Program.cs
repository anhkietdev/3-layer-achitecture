﻿using BankManagement.Presentation;

Console.WriteLine("****************Bank Management****************");
Console.WriteLine("::Login Page::");
string userName = null, password = null;

Console.Write("Username: ");
userName = Console.ReadLine();

if (userName != "")
{
    Console.Write("Password: ");
    password = Console.ReadLine();
}

if (userName == "admin" && password == "admin")
{
    int mainMenuChoice = -1;

    do
    {
        Console.WriteLine("\n:::MAIN MENU:::");
        Console.WriteLine("1. Customers");
        Console.WriteLine("2. Accounts");
        Console.WriteLine("3. Funds Transfer");
        Console.WriteLine("4. Funds Transfer Statement");
        Console.WriteLine("5. Account Statement");
        Console.WriteLine("0. Exit");
        Console.Write("Enter your choice: ");
        mainMenuChoice = int.Parse(Console.ReadLine());

        switch (mainMenuChoice)
        {
            case 1:
                CustomerMenu();
                break;
            case 2:
                AccountMenu();
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            default:
                break;
        }
    } while (mainMenuChoice != 0);
}
else
{
    Console.WriteLine("Invalid username or password");
}

Console.WriteLine("Thank you! See you again.");
Console.ReadKey();

static void CustomerMenu()
{
    int customerMenuChoice = -1;

    do
    {
        Console.WriteLine("\n:::CUSTOMER MENU:::");
        Console.WriteLine("1. Add Customer");
        Console.WriteLine("2. Delte Customer");
        Console.WriteLine("3. Update Customer");
        Console.WriteLine("4. Search Customers");
        Console.WriteLine("5. View Customer");
        Console.WriteLine("0. Back to Main Menu");

        Console.Write("Enter your choice: ");
        customerMenuChoice = int.Parse(Console.ReadLine());

        switch (customerMenuChoice)
        {
            case 1:
                CustomerPresentation.AddCustomer();
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                CustomerPresentation.ViewCustomers();
                break;
            default:
                break;
        }

    } while (customerMenuChoice != 0);
}
static void AccountMenu()
{
    int accountMenuChoice = -1;

    do
    {
        Console.WriteLine("\n:::ACCOUNT MENU:::");
        Console.WriteLine("1. Add Account");
        Console.WriteLine("2. Delte Account");
        Console.WriteLine("3. Update Account");
        Console.WriteLine("4. Search Account");
        Console.WriteLine("5. View Accounts");
        Console.WriteLine("0. Back to Main Menu");

        Console.Write("Enter your choice: ");
        accountMenuChoice = int.Parse(Console.ReadLine());

        switch (accountMenuChoice)
        {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;
        }

    } while (accountMenuChoice != 0);
}