﻿using BankManagement.Entities.Contracts;
using BankManagement.Exceptions;

namespace BankManagement.Entities
{
    /// <summary>
    /// Represents customer of the bank.
    /// </summary>
    public class Customer : ICustomer, ICloneable
    {
        #region Private fields
        private Guid _customerId;
        private long _customerCode;
        private string _customerName;
        private string _address;
        private string _city;
        private string _country;
        private string _phone;
        #endregion

        #region Properties  

        /// <summary>
        /// Gets or sets the Guid of the customer for unique identification.
        /// </summary>
        public Guid CustomerId { get => _customerId; set => _customerId = value; }

        /// <summary>
        /// Gets or sets the code associated with the customer.
        /// </summary>
        public long CustomerCode
        {
            get => _customerCode;
            set
            {
                //customer code should be positive
                if (value > 0)
                {
                    _customerCode = value;
                }
                else
                {
                    throw new CustomerExceptions("Customer code should be positive only");
                }
            }
        }

        /// <summary>
        /// Gets or sets the name of the customer.
        /// </summary>
        public string CustomerName
        {
            get => _customerName;
            set
            {
                if (value.Length <= 40 && string.IsNullOrEmpty(value) == false)
                {
                    _customerName = value;
                }
                else
                {
                    throw new CustomerExceptions("Customer name should not be null and less than 40 characters");
                }
            }
        }

        /// <summary>
        /// Gets or sets the address of the customer.
        /// </summary>
        public string Address { get => _address; set => _address = value; }

        /// <summary>
        /// Gets or sets the city where the customer is located.
        /// </summary>
        public string City { get => _city; set => _city = value; }

        /// <summary>
        /// Gets or sets the country where the customer is located.
        /// </summary>
        public string Country { get => _country; set => _country = value; }

        /// <summary>
        /// Gets or sets the phone number associated with the customer.
        /// </summary>
        public string Phone
        {
            get => _phone;
            set
            {
                if (value.Length == 11)
                {
                    _phone = value;
                }
                else
                {
                    throw new CustomerExceptions("Customer Phonenumber should be a 11-digit number");
                }
            }
        }

        #region Methods
        public object Clone()
        {
            return new Customer()
            {
                CustomerId = this.CustomerId,
                CustomerName = this.CustomerName,
                Address = this.Address,
                City = this.City,
                Country = this.Country,
                Phone = this.Phone
            };
        }
        #endregion
        #endregion
    }
}
