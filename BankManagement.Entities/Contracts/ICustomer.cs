﻿namespace BankManagement.Entities.Contracts
{
    /// <summary>
    /// Represents the interface of customer entity.
    /// </summary>
    public interface ICustomer
    {
        #region Properties
        Guid CustomerId { get; set; }
        long CustomerCode { get; set; }
        string CustomerName { get; set; }
        string Address { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string Phone { get; set; }
        #endregion
    }
}
